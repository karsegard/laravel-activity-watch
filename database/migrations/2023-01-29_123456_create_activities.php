<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aw_id')->index();
            $table->foreignId('user_id')->nullable();
            $table->decimal('duration');
            $table->foreignId('channel_id')->index();
            $table->json('data')->nullable();
            $table->timestamp('date')->index();
            $table->timestamps();

        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('activities');
     
        Schema::enableForeignKeyConstraints();
    }
};