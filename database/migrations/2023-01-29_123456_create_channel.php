<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('channels', function (Blueprint $table) {
            $table->id();
            $table->string('aw_id');
            $table->string('name');
            $table->string('client');
            $table->string('type');
            $table->string('host');
            $table->timestamp('created')->nullable();
            $table->timestamp('updated')->nullable();
            $table->timestamps();

        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('channels');
     
        Schema::enableForeignKeyConstraints();
    }
};