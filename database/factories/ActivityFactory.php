<?php

namespace KDA\Laravel\ActivityWatch\Database\Factories;

use KDA\Laravel\ActivityWatch\Models\Activity;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\ActivityWatch\Models\Channel;
use KDA\Tests\Models\User;

class ActivityFactory extends Factory
{
    protected $model = Activity::class;

    public function definition()
    {
        return [
            'aw_id'=>$this->faker->randomNumber(),
            'user_id'=>User::factory(),
            'duration'=>$this->faker->numberBetween(0,5000),
            'channel_id'=>Channel::factory(),
            'data'=> [],
            'date'=>$this->faker->date(),
        ];
    }
}
