<?php

namespace KDA\Laravel\ActivityWatch\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\ActivityWatch\Models\Channel;

class ChannelFactory extends Factory
{
    protected $model = Channel::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->word(),
            'aw_id'=>$this->faker->slug(1),
            'client'=>$this->faker->slug(1),
            'type'=>$this->faker->slug(1),
            'host'=>$this->faker->word(),
            'created'=>$this->faker->date(),
            'updated'=>$this->faker->date(),

        ];
    }
}
