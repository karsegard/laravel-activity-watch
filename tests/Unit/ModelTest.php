<?php

namespace KDA\Tests\Unit;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\ActivityWatch\Facades\ActivityWatch;
use KDA\Laravel\ActivityWatch\Models\Activity;
use KDA\Laravel\ActivityWatch\Models\Channel;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_activity()
  {
    $o = Activity::factory()->create([]);
    $this->assertNotNull($o);
  }

  /** @test */
  function can_create_channel()
  {
    $o = Channel::factory()->create([]);
    $this->assertNotNull($o);
  }
/** @test */
function can_import_data()
{
  
  ActivityWatch::import( new Carbon('2023-01-27'),new Carbon ('2023-01-28'));
 // dump(Channel::all());
 dump(Activity::first());
}




  
}