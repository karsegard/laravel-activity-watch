<?php

namespace KDA\Laravel\ActivityWatch\Rest;

use KDA\Rest\Collections\NestedArray;

class Buckets extends NestedArray
{

    public function __construct($data){
        $data = array_values($data);
        parent::__construct($data);
    }

    public function buckets(){
        return $this->data;
    }
}