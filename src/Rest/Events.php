<?php

namespace KDA\Laravel\ActivityWatch\Rest;

use KDA\Rest\Collections\NestedArray;

class Events extends NestedArray
{
    static public function getAccessorKey(){
        return 'events';
    }
    static public function getClass(){
        return Event::class;
    }

    public function events(){
        return $this->data;
    }
}