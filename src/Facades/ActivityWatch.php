<?php

namespace KDA\Laravel\ActivityWatch\Facades;

use Illuminate\Support\Facades\Facade;

class ActivityWatch extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
