<?php

namespace KDA\Laravel\ActivityWatch;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

use KDA\Laravel\Concerns\CanIgnoreMigrations;
use KDA\Laravel\ActivityWatch\ImportTask\ImportTask;

//use Illuminate\Support\Facades\Blade;
class ActivityWatch
{
    use CanIgnoreMigrations;
    public function importTask()
    {
       return app(ImportTask::class);
      
    }
}
