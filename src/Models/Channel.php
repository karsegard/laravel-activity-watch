<?php

namespace KDA\Laravel\ActivityWatch\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\ActivityWatch\Database\Factories\ChannelFactory;

class Channel extends Model
{
    use HasFactory;

    protected $fillable = [
        'aw_id',
        'name',
        'client',
        'type',
        'host',
        'created',
        'updated'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       'created'=>'datetime',
       'updated'=>'datetime'
    ];

   
    protected static function newFactory()
    {
        return  ChannelFactory::new();
    }

}
