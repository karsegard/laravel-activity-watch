<?php

namespace KDA\Laravel\ActivityWatch\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\ActivityWatch\Database\Factories\ActivityFactory;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'aw_id',
        'user_id',
        'duration',
        'channel_id',
        'data',
        'date',
    ];

    protected $appends = [];

    protected $casts = [
        'data'=>'json',
        'duration'=>'float'
    ];


    protected static function newFactory()
    {
        return  ActivityFactory::new();
    }

    public function channel(){
        return $this->belongsTo(Channel::class,'channel_id');
    }
}
