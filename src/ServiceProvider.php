<?php

namespace KDA\Laravel\ActivityWatch;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\ActivityWatch\Facades\ActivityWatch as Facade;
use KDA\Laravel\ActivityWatch\ActivityWatch as Library;
use KDA\Laravel\ActivityWatch\Commands\ImportCommand;
use KDA\Laravel\Contracts\CanIgnoreMigration;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasDumps;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasMigration;

class ServiceProvider extends PackageServiceProvider implements CanIgnoreMigration
{
    use HasCommands;
    use HasConfig;
    use HasLoadableMigration;
    use HasMigration;
    use HasDumps;

    protected $_commands = [
        ImportCommand::class
    ];

    protected $dumps = [
        'channels',
        'activities'
    ];
    protected $packageName = 'laravel-activity-watch';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    // trait \KDA\Laravel\Traits\HasConfig; 
    //    registers config file as 
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';
    protected $configs = [
        'kda/activity-watch.php'  => 'kda.activity-watch'
    ];
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }

    public function shouldLoadMigration(){
        return Facade::shouldRunMigrations();
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
