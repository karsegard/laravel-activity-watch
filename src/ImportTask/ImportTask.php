<?php

namespace KDA\Laravel\ActivityWatch\ImportTask;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Closure;
use KDA\Laravel\ActivityWatch\ImportTask\Concerns\EvaluatesClosure;
use KDA\Rest\Facades\Rest;
use KDA\Laravel\ActivityWatch\Models\Activity;
use KDA\Laravel\ActivityWatch\Models\Channel;
use KDA\Laravel\ActivityWatch\Rest\Buckets;
use KDA\Laravel\ActivityWatch\Rest\Events;
class ImportTask{
    use EvaluatesClosure;

    protected $api = "http://localhost:5600/api/0/";
    protected $from ;
    protected $until;
    protected string | array | null $bucket = null;

    protected ?Closure $process_bucket_event = null;
    protected ?Closure $process_event_event = null;

    public function onBucket($callback):static{
        $this->process_bucket_event = $callback;
        return $this;
    }

    public function onEvent($callback):static{
        $this->process_event_event = $callback;
        return $this;

    }

    public function from($from):static 
    {
        $this->from = $from;
        return $this;
    }

    public function until($until):static 
    {
        $this->until = $until;
        return $this;
    }

    public function bucket($bucket):static
    {   
       
        $this->bucket = $bucket;
        return $this;
    }

    public function api($api):static
    {   
        $this->api = $api;
        return $this;
    }
    
    public function getFrom():Carbon{
        return new Carbon($this->from);
    }

    public function getUntil():Carbon{
        return new Carbon($this->until);

    }

    public function getBucket():?array{
        $bucket = $this->bucket;
        if(is_string($bucket)){
            $bucket = [$bucket];
        }
        return $bucket;
    }

    public function getApi(){
        return $this->api;
    }

    public function processBucketEvent($data){
        $this->evaluate($this->process_bucket_event,['event'=>$data]);
    }
    public function processEventEvent($data){
        $this->evaluate($this->process_event_event,['event'=>$data]);
    }
    public function import()
    {
        $api = $this->getApi();
        $client = Rest::getClient($api)->get('buckets');

        $filter = $this->getBucket();

        $buckets = $client->getResponse(Buckets::class)->body->buckets();
        foreach ($buckets as $bucket) {
            if($filter && !in_array($bucket['id'],$filter)){
                continue;
            }

            $channel = Channel::updateOrCreate(
                ['aw_id' => $bucket['id']],
                [
                    'aw_id' => $bucket['id'],
                    'name' => $bucket['id'],
                    'client' => $bucket['client'],
                    'type' => $bucket['type'],
                    'host' => $bucket['hostname'],
                    'created' => $bucket['created'],
                    'updated' => $bucket['created'],
                ]
            );
            $this->processBucketEvent($channel);
            
            /*
            $import_from = $channel->updated;

            $start = $import_from;
            $end = Carbon::now(); 
            $period = CarbonPeriod::create($start, '1 '.'day', $end);
    
            $intervals = [];
            foreach ($period as $month) {
                $intervals[$month->format("W-Y")] = $month;
            }
  */
            $intervals = [
                '-'=> [
                    'start'=>$this->getFrom(),
                    'end'=>$this->getUntil()
                ]
                ];
            foreach($intervals as $range){
                ['start'=>$start,'end'=>$end]= $range;
                $result = Rest::getClient($api)->get('buckets/' . $channel->aw_id . '/events', [
                    'start' => $start->format('Y-m-d'),
                    'end' => $end->format('Y-m-d')
                ])->getResponse(Events::class)->body->events;
                
                $event = $result->first();
                
                foreach($result as $event){
                    $was_afk = false;
                    if($channel->client!="aw-watcher-afk"){
                        
                       
                        $afk = Activity::select()->addSelect(\DB::raw(' DATE_ADD(date, INTERVAL duration second) as date_end'))
                        ->whereHas('channel',function($q){
                            $q->where('client','aw-watcher-afk');
                        })
                        ->whereRaw('DATE(date) = ? ', [(new Carbon($event->timestamp))->format('Y-m-d')])
                        ->where('date','<=',Carbon::parse($event->timestamp))
                        ->orderBy('date','desc')->orderBy('date_end','desc')
                       
                       
                        ->first();

                        $was_afk = $afk ? $afk->data['status']=='afk' : false;
                   //     dump($afk->date,$afk->date_end,$afk->data['status'],$event->timestamp,"---------");
                       
                    }
                    $changes =  [
                        'duration'=>$event->duration,
                        'channel_id'=>$channel->id,
                        'data'=>array_merge($event->body['data'],['afk'=>$was_afk]),
                        'date'=>$event->timestamp,
                    ];
                    $activity = Activity::firstOrCreate(
                        ['aw_id'=>$event->id],
                      $changes
                    );

                   /* $activity->fill($changes);
                    $activity->save();*/
                  /* dump($activity);
                    foreach($changes as $k=>$v){
                        $activity->$k = $v;
                    }
                    dump($activity);
                    dump($activity->isDirty());
                    */
                     $this->processEventEvent($activity);

                }
                
                unset($result);
            }
            //      dump($result);
            //      dump(get_class($result->getResponse(Events::class)->body));
            //    dump($result->getResponse(Events::class)->body);
            //http://localhost:5600/api/0/buckets/aw-watcher-afk_daoloth/events
        }
    }
}