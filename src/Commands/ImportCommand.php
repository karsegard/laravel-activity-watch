<?php

namespace KDA\Laravel\ActivityWatch\Commands;

use Illuminate\Console\Command;
use KDA\Laravel\ActivityWatch\Facades\ActivityWatch;
use KDA\Laravel\BackupServer\BackupTask\Enums\BackupStatus;
use KDA\Laravel\BackupServer\Facades\BackupServer;

class ImportCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:activity-watch:import {start} {end} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $start = $this->argument('start');
        $end = $this->argument('end');

        ActivityWatch::importTask()
            ->from($start)
            ->until($end)
           // ->bucket('aw-watcher-afk_daoloth')
            ->onBucket(function($event){
                
                $this->info ($event->name);
            })
            ->onEvent(function($event){
                $app = $event->data['app'] ?? 'unkown';
               /// $this->info ("{$app} {$event->duration}");
               $log= '.';
               if($event->wasRecentlyCreated){
                $log = 'C';
               }elseif($event->wasChanged()){
                $log='U';
               }
               $this->output->write($log, false);
            })
            ->import();
    }
}
